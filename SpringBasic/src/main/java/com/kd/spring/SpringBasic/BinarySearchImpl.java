package com.kd.spring.SpringBasic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BinarySearchImpl {
	
	@Autowired
	SortAlgorithm sa;
	
	public BinarySearchImpl(SortAlgorithm sa) {
		super();
		this.sa = sa;
	}

	public int binarySearch(int [] numbers, int number) {
		
		int[] sortedNumbers = sa.sort(numbers);
		System.out.println(sa);
		return 3;
	}

}
