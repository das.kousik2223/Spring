package com.kd.spring.SpringBasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringBasicApplication {

	public static void main(String[] args) {
		
		
		ApplicationContext ac = SpringApplication.run(SpringBasicApplication.class, args);
		BinarySearchImpl bsi = ac.getBean(BinarySearchImpl.class);
		//BinarySearchImpl bsi = new BinarySearchImpl(new QuickSort());
		int result = bsi.binarySearch(new int[] {124, 6},3);
		System.out.println(result);
	}

}
