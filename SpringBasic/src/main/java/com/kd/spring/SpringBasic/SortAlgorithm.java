package com.kd.spring.SpringBasic;

public interface SortAlgorithm {
	public int [] sort(int [] numbers);
}
